package com.wulikic.venues.usecase;

import com.wulikic.venues.data.model.SearchByLocationNameResponse;

import org.junit.Test;

import static org.mockito.Mockito.mock;

/**
 * Created by vesna on 19/08/2017.
 */
public class NearbyVenueMapperTest {

    @Test(expected = NullPointerException.class)
    public void testWhenSearchResponseIsEmpty_shouldThrowException() throws Exception {
        // given
        VenueMapper venueMapper = mock(VenueMapper.class);
        NearbyVenueMapper nearbyVenueMapper = new NearbyVenueMapper(venueMapper);
        SearchByLocationNameResponse response = new SearchByLocationNameResponse();

        // when
        nearbyVenueMapper.apply(response);

        // then should throw Exception
    }


}