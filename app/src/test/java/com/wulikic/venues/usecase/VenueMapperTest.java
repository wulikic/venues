package com.wulikic.venues.usecase;

import com.wulikic.venues.data.model.LocationData;
import com.wulikic.venues.data.model.VenueData;
import com.wulikic.venues.model.Venue;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by vesna on 19/08/2017.
 */
public class VenueMapperTest {

    @Test
    public void testVenueNameMapping() throws Exception {
        // given
        VenueData venueData = mock(VenueData.class);
        when(venueData.getName()).thenReturn("fakeName");
        VenueMapper venueMapper = new VenueMapper();

        // when
        Venue venue = venueMapper.mapToVenue(venueData);

        // then
        assertEquals("fakeName", venue.getName());
    }

    @Test
    public void testVenueLocationMappingWhenListNull() throws Exception {
        // given
        LocationData locationData = mock(LocationData.class);
        VenueData venueData = mock(VenueData.class);
        VenueMapper venueMapper = new VenueMapper();

        when(locationData.getFormattedAddress()).thenReturn(null);
        when(venueData.getLocation()).thenReturn(locationData);

        // when
        Venue venue = venueMapper.mapToVenue(venueData);

        // then
        assertEquals(null, venue.getAddressLine1());
        assertEquals(null, venue.getAddressLine2());
        assertEquals(null, venue.getAddressLine3());
    }

    @Test
    public void testVenueLocationMappingWhenListEmpty() throws Exception {
        // given
        LocationData locationData = mock(LocationData.class);
        VenueData venueData = mock(VenueData.class);
        VenueMapper venueMapper = new VenueMapper();

        when(locationData.getFormattedAddress()).thenReturn(new ArrayList<String>(0));
        when(venueData.getLocation()).thenReturn(locationData);

        // when
        Venue venue = venueMapper.mapToVenue(venueData);

        // then
        assertEquals(null, venue.getAddressLine1());
        assertEquals(null, venue.getAddressLine2());
        assertEquals(null, venue.getAddressLine3());
    }

    @Test
    public void testVenueLocationMappingWhenOnlyOneAddressLine() throws Exception {
        // given
        List<String> dummyLocation = new ArrayList<>();
        dummyLocation.add("line1");

        LocationData locationData = mock(LocationData.class);
        VenueData venueData = mock(VenueData.class);
        VenueMapper venueMapper = new VenueMapper();

        when(locationData.getFormattedAddress()).thenReturn(dummyLocation);
        when(venueData.getLocation()).thenReturn(locationData);

        // when
        Venue venue = venueMapper.mapToVenue(venueData);

        // then
        assertEquals("line1", venue.getAddressLine1());
        assertEquals(null, venue.getAddressLine2());
        assertEquals(null, venue.getAddressLine3());
    }

}