package com.wulikic.venues.model;

import java.util.List;

/**
 * Created by vesna on 19/08/2017.
 */

public class NearbyVenues {

    private String location;
    private List<Venue> venues;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }
}
