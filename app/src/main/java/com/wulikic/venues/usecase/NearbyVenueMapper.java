package com.wulikic.venues.usecase;

import com.wulikic.venues.data.model.GeocodeData;
import com.wulikic.venues.data.model.ResponseData;
import com.wulikic.venues.data.model.SearchByLocationNameResponse;
import com.wulikic.venues.data.model.VenueData;
import com.wulikic.venues.data.model.VenueGroupData;
import com.wulikic.venues.data.model.VenueItemData;
import com.wulikic.venues.model.NearbyVenues;
import com.wulikic.venues.model.Venue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Created by vesna on 19/08/2017.
 */

public class NearbyVenueMapper implements Function<SearchByLocationNameResponse, NearbyVenues> {

    private VenueMapper venueMapper;

    public NearbyVenueMapper(@android.support.annotation.NonNull VenueMapper venueMapper) {
        this.venueMapper = venueMapper;
    }

    @Override
    public NearbyVenues apply(@NonNull SearchByLocationNameResponse searchResponse) throws Exception {
        ResponseData responseData = searchResponse.getResponse();
        NearbyVenues nearByVenues = new NearbyVenues();
        String location = getLocationNameFromResponse(responseData);
        nearByVenues.setLocation(location);
        List<Venue> venueList = getVenueList(responseData);
        nearByVenues.setVenues(venueList);
        return nearByVenues;
    }

    private List<Venue> getVenueList(@android.support.annotation.NonNull ResponseData responseData) {
        if (responseData.getTotalResults() > 0) {
            List<VenueData> venueDataList = getAllNonNullVenueData(responseData.getGroups());
            List<Venue> venueList = new ArrayList<>();
            for (VenueData venueData : venueDataList) {
                Venue venue = venueMapper.mapToVenue(venueData);
                venueList.add(venue);
            }
            return venueList;
        } else {
            return Collections.emptyList();
        }
    }

    private List<VenueData> getAllNonNullVenueData(List<VenueGroupData> venueGroupDataList) {
        List<VenueData> venueDataList = new ArrayList<>();
        for (VenueGroupData venueGroup : venueGroupDataList) {
            if (venueGroup != null) {
                for (VenueItemData venueItemData : venueGroup.getVenueItems()) {
                    if (venueItemData != null) {
                        VenueData venueData = venueItemData.getVenueData();
                        if (venueData != null) {
                            venueDataList.add(venueData);
                        }
                    }
                }
            }
        }
        return venueDataList;
    }

    private String getLocationNameFromResponse(@android.support.annotation.NonNull ResponseData responseData) {
        GeocodeData geocodeData = responseData.getGeocode();
        if (geocodeData != null) {
            return geocodeData.getDisplayString();
        } else {
            return null;
        }
    }

}
