package com.wulikic.venues.usecase;

/**
 * Created by vesna on 19/08/2017.
 */

public class SearchFailedException extends Exception {

    public SearchFailedException(String message) {
        super(message);
    }

    public SearchFailedException() {

    }

}
