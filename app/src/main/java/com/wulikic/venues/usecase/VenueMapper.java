package com.wulikic.venues.usecase;

import android.support.annotation.NonNull;

import com.wulikic.venues.data.model.LocationData;
import com.wulikic.venues.data.model.VenueData;
import com.wulikic.venues.model.Venue;

import java.util.List;

/**
 * Created by vesna on 19/08/2017.
 */

public class VenueMapper {

    public Venue mapToVenue(@NonNull VenueData venueData) {
        Venue venue = new Venue();
        venue.setName(venueData.getName());
        venue.setUrl(venueData.getUrl());
        venue.setPhone(venue.getPhone());
        venue.setRating(venueData.getRating());

        LocationData locationData = venueData.getLocation();
        if (locationData != null && locationData.getFormattedAddress() != null) {
            List<String> address = locationData.getFormattedAddress();
            if (address != null) {
                if (address.size() > 0) {
                    venue.setAddressLine1(address.get(0));
                }
                if (address.size() > 1) {
                    venue.setAddressLine2(address.get(1));
                }
                if (address.size() > 2) {
                    venue.setAddressLine3(address.get(2));
                }
            }
        }
        return venue;
    }

}
