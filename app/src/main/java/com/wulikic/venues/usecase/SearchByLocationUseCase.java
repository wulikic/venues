package com.wulikic.venues.usecase;

import android.support.annotation.NonNull;

import com.wulikic.venues.data.repository.SearchRepository;
import com.wulikic.venues.model.NearbyVenues;

import io.reactivex.Observable;

/**
 * Created by vesna on 19/08/2017.
 */

public class SearchByLocationUseCase {

    private SearchRepository searchRepository;
    private NearbyVenueMapper nearbyVenueMapper;

    public SearchByLocationUseCase(@NonNull SearchRepository searchRepository, @NonNull NearbyVenueMapper nearbyVenueMapper) {
        this.searchRepository = searchRepository;
        this.nearbyVenueMapper = nearbyVenueMapper;
    }

    public Observable<NearbyVenues> execute(@NonNull final String locationName) {
        return searchRepository.searchByName(locationName).map(nearbyVenueMapper);
    }

}
