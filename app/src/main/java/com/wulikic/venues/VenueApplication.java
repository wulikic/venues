package com.wulikic.venues;

import android.app.Application;

import com.wulikic.venues.di.component.ApplicationComponent;
import com.wulikic.venues.di.component.DaggerApplicationComponent;
import com.wulikic.venues.di.module.GsonModule;
import com.wulikic.venues.di.module.RepositoryModule;
import com.wulikic.venues.di.module.RetrofitModule;
import com.wulikic.venues.di.module.UseCaseModule;

/**
 * Created by vesna on 19/08/2017.
 */

public class VenueApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .useCaseModule(new UseCaseModule())
                .repositoryModule(new RepositoryModule())
                .retrofitModule(new RetrofitModule())
                .gsonModule(new GsonModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
