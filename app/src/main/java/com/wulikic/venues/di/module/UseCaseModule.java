package com.wulikic.venues.di.module;

import android.support.annotation.NonNull;

import com.wulikic.venues.data.repository.SearchRepository;
import com.wulikic.venues.usecase.NearbyVenueMapper;
import com.wulikic.venues.usecase.SearchByLocationUseCase;
import com.wulikic.venues.usecase.VenueMapper;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 19/08/2017.
 */

@Module
public class UseCaseModule {

    @Provides
    SearchByLocationUseCase provideSearchByLocationUseCase(@NonNull SearchRepository searchRepository, @NonNull NearbyVenueMapper nearbyVenueMapper) {
        return new SearchByLocationUseCase(searchRepository, nearbyVenueMapper);
    }

    @Provides
    NearbyVenueMapper nearbyVenueMapper(@NonNull VenueMapper venueMapper) {
        return new NearbyVenueMapper(venueMapper);
    }

    @Provides
    VenueMapper provideVenueMapper() {
        return new VenueMapper();
    }

}
