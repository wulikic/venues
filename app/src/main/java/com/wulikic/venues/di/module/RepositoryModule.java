package com.wulikic.venues.di.module;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.wulikic.venues.data.FoursquareApi;
import com.wulikic.venues.data.repository.FoursquareSearchRepository;
import com.wulikic.venues.data.repository.SearchRepository;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by vesna on 19/08/2017.
 */

@Module
public class RepositoryModule {

    @Provides
    SearchRepository provideSearchRepository(@NonNull FoursquareApi api, @NonNull Gson gson) {
        return new FoursquareSearchRepository(api, gson);
    }

    @Provides
    FoursquareApi provideApi(@NonNull Retrofit retrofit) {
        return retrofit.create(FoursquareApi.class);
    }

}
