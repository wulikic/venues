package com.wulikic.venues.di.module;

import android.support.annotation.NonNull;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.wulikic.venues.data.repository.FoursquareApiCallInterceptor;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vesna on 19/08/2017.
 */

@Module
public class RetrofitModule {

    @Provides
    Retrofit provideRetrofit(@NonNull OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com")
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    OkHttpClient provideOkHttpClient(@NonNull FoursquareApiCallInterceptor apiCallInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(apiCallInterceptor)
                .build();
    }

    @Provides
    FoursquareApiCallInterceptor provideApiCallInterceptor() {
        return new FoursquareApiCallInterceptor();
    }

}
