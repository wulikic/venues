package com.wulikic.venues.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vesna on 19/08/2017.
 */

@Module
public class GsonModule {

    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder().create();
    }
}
