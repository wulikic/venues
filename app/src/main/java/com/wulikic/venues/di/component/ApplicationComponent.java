package com.wulikic.venues.di.component;

import com.wulikic.venues.di.module.GsonModule;
import com.wulikic.venues.di.module.RepositoryModule;
import com.wulikic.venues.di.module.RetrofitModule;
import com.wulikic.venues.di.module.UseCaseModule;
import com.wulikic.venues.presentation.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by vesna on 19/08/2017.
 */

@Singleton
@Component(modules = {
        UseCaseModule.class,
        RepositoryModule.class,
        RetrofitModule.class,
        GsonModule.class
})
public interface ApplicationComponent {

    void inject(MainActivity mainActivity);

}
