package com.wulikic.venues.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wulikic.venues.R;
import com.wulikic.venues.model.Venue;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vesna on 19/08/2017.
 */

class VenueListAdapter extends RecyclerView.Adapter<VenueListAdapter.VenueHolder> {

    private List<Venue> items;

    public void setItems(List<Venue> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public VenueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.venue_item, parent, false);
        return new VenueHolder(view);
    }

    @Override
    public void onBindViewHolder(VenueHolder holder, int position) {
        Venue item = items.get(position);
        holder.name.setText(item.getName());
        holder.address.setText(item.getAddressLine1());
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }


    static class VenueHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.address)
        TextView address;

        VenueHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
