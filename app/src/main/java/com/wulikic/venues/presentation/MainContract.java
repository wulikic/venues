package com.wulikic.venues.presentation;

import android.support.annotation.NonNull;

import com.wulikic.venues.model.Venue;

import java.util.List;

/**
 * Created by vesna on 19/08/2017.
 */

public class MainContract {

    interface View {
        void showVenues(List<Venue> venueList);
        void showNoNearbyVenues(String location);
        void showSearchError(String error);
        void showVenueCount(String location, int venueCount);
        void hideVenues();
    }

    interface Presenter {
        void attach(@NonNull View view);
        void detach();
        void terminate();
        void onLocationEntered(String location);
    }

}
