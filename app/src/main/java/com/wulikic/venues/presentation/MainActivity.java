package com.wulikic.venues.presentation;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.TextView;

import com.wulikic.venues.R;
import com.wulikic.venues.VenueApplication;
import com.wulikic.venues.model.Venue;
import com.wulikic.venues.usecase.SearchByLocationUseCase;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.location)
    TextView locationView;

    @BindView(R.id.list_container)
    RecyclerView recyclerView;

    @Inject
    SearchByLocationUseCase searchByLocationUseCase;

    private VenueListAdapter adapter;
    private MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((VenueApplication) getApplication()).getApplicationComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new VenueListAdapter();
        recyclerView.setAdapter(adapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.onLocationEntered(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        if (savedInstanceState == null) {
            presenter = new MainPresenter(searchByLocationUseCase);
        } else {
            presenter = (MainContract.Presenter) getLastCustomNonConfigurationInstance();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attach(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detach();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            presenter.terminate();
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }

    @Override
    public void showVenues(List<Venue> venueList) {
        adapter.setItems(venueList);
    }

    @Override
    public void showNoNearbyVenues(String location) {
        locationView.setText("No venues near " + location);
        locationView.setVisibility(View.VISIBLE);
        locationView.setBackgroundColor(Color.LTGRAY);
        adapter.setItems(null);
    }

    @Override
    public void showSearchError(String error) {
        locationView.setText(error);
        locationView.setVisibility(View.VISIBLE);
        locationView.setBackgroundColor(Color.YELLOW);
    }

    @Override
    public void showVenueCount(String location, int count) {
        locationView.setText("Found " + count + " venues near " + location);
        locationView.setVisibility(View.VISIBLE);
        locationView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void hideVenues() {
        adapter.setItems(null);
    }

}
