package com.wulikic.venues.presentation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.wulikic.venues.model.NearbyVenues;
import com.wulikic.venues.model.Venue;
import com.wulikic.venues.usecase.SearchByLocationUseCase;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vesna on 19/08/2017.
 */

class MainPresenter implements MainContract.Presenter {

    @NonNull
    private SearchByLocationUseCase searchByLocationUseCase;

    private Disposable disposable = null;
    private NearbyVenues nearbyVenuesCache;
    private String errorCache;

    @Nullable
    private MainContract.View view;

    MainPresenter(@NonNull SearchByLocationUseCase searchByLocationUseCase) {
        this.searchByLocationUseCase = searchByLocationUseCase;
    }

    @Override
    public void attach(@NonNull MainContract.View view) {
        this.view = view;
        updateView();
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void terminate() {
        detach();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    @Override
    public void onLocationEntered(String location) {
        if (disposable != null && !disposable.isDisposed()) {
            return;
        }
        disposable = searchByLocationUseCase.execute(location)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate(new Action() {
                    @Override
                    public void run() throws Exception {
                        if (disposable != null && !disposable.isDisposed()) {
                            disposable.dispose();
                        }
                    }
                })
                .subscribeWith(new DisposableObserver<NearbyVenues>() {
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull NearbyVenues nearbyVenues) {
                        nearbyVenuesCache = nearbyVenues;
                        errorCache = null;
                        updateView();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        nearbyVenuesCache = null;
                        errorCache = e.getMessage();
                        updateView();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void updateView() {
        if (view != null) {
            if (errorCache != null) {
                view.showSearchError(errorCache);
                view.hideVenues();
            } else if (nearbyVenuesCache != null) {
                int count = venueCount();
                if (count > 0) {
                    view.showVenueCount(nearbyVenuesCache.getLocation(), count);
                    view.showVenues(nearbyVenuesCache.getVenues());
                } else {
                    view.showNoNearbyVenues(nearbyVenuesCache.getLocation());
                }
            }
        }
    }

    private int venueCount() {
        List<Venue> venues = nearbyVenuesCache.getVenues();
        if (venues == null || venues.size() < 1) {
            return 0;
        }
        return nearbyVenuesCache.getVenues().size();
    }

}
