package com.wulikic.venues.data.repository;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.wulikic.venues.data.FoursquareApi;
import com.wulikic.venues.data.model.SearchByLocationNameResponse;
import com.wulikic.venues.usecase.SearchFailedException;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by vesna on 19/08/2017.
 */

public class FoursquareSearchRepository implements SearchRepository {

    private FoursquareApi foursquareApi;
    private Gson gson;

    public FoursquareSearchRepository(@NonNull FoursquareApi foursquareApi, @NonNull Gson gson) {
        this.foursquareApi = foursquareApi;
        this.gson = gson;
    }

    @Override
    public Observable<SearchByLocationNameResponse> searchByName(@NonNull String locationName) {
        return foursquareApi.searchByLocationName(locationName)
                .onErrorResumeNext(new Function<Throwable, ObservableSource<? extends SearchByLocationNameResponse>>() {
                    @Override
                    public ObservableSource<? extends SearchByLocationNameResponse> apply(@io.reactivex.annotations.NonNull Throwable throwable) throws Exception {
                        SearchByLocationNameResponse error = null;
                        if (throwable instanceof HttpException) {
                            HttpException retrofitException = (HttpException) throwable;
                            Response response = retrofitException.response();
                            if (response != null) {
                                ResponseBody responseErrorBody = response.errorBody();
                                try {
                                    error = gson.fromJson(responseErrorBody.charStream(), SearchByLocationNameResponse.class);
                                } catch (Exception exception) {
                                }
                            }
                        }
                        if (error != null && error.getMeta() != null && error.getMeta().getErrorDetail() != null) {
                            return Observable.error(new SearchFailedException(error.getMeta().getErrorDetail()));
                        } else {
                            return Observable.error(throwable);
                        }
                    }
        });
    }

}
