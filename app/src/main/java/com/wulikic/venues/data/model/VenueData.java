package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vesna on 19/08/2017.
 */

public class VenueData {

    @SerializedName("name")
    String name;

    @SerializedName("location")
    LocationData location;

    @SerializedName("url")
    String url;

    @SerializedName("rating")
    float rating;

    public String getName() {
        return name;
    }

    public LocationData getLocation() {
        return location;
    }

    public String getUrl() {
        return url;
    }

    public float getRating() {
        return rating;
    }

}
