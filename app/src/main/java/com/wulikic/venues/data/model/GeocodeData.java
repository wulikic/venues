package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vesna on 19/08/2017.
 */

public class GeocodeData {

    @SerializedName("displayString")
    String displayString;

    public String getDisplayString() {
        return displayString;
    }

}
