package com.wulikic.venues.data.repository;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by vesna on 19/08/2017.
 */

public class FoursquareApiCallInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        HttpUrl originalUrl = originalRequest.url();
        HttpUrl newUrl = originalUrl.newBuilder()
                .addQueryParameter("v", "20170101")
                .addQueryParameter("client_id", "ERZOTLQK2YKDEBU24OU3OQXN5DOGJWYCHLHCN30WABHVD5HO")
                .addQueryParameter("client_secret", "DHQJYNYNADYG4WSI2TLHXKSGFDH0LL04XIVCSIX3C0NWEZVU")
                .build();
        Request newRequest = originalRequest.newBuilder()
                .url(newUrl)
                .build();
        return chain.proceed(newRequest);
    }

}
