package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vesna on 19/08/2017.
 */

public class MetaData {

    @SerializedName("code")
    int code;

    @SerializedName("errorDetail")
    String errorDetail;

    public String getErrorDetail() {
        return errorDetail;
    }

}
