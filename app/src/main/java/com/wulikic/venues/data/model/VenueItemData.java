package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vesna on 19/08/2017.
 */

public class VenueItemData {

    @SerializedName("venue")
    VenueData venueData;

    public VenueData getVenueData() {
        return venueData;
    }
}
