package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vesna on 19/08/2017.
 */

public class SearchByLocationNameResponse {

    @SerializedName("meta")
    MetaData meta;

    @SerializedName("response")
    ResponseData response;

    public MetaData getMeta() {
        return meta;
    }

    public ResponseData getResponse() {
        return response;
    }

}
