package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vesna on 19/08/2017.
 */

public class LocationData {

    @SerializedName("formattedAddress")
    List<String> formattedAddress;

    public List<String> getFormattedAddress() {
        return formattedAddress;
    }

}
