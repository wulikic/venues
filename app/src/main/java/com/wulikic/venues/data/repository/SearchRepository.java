package com.wulikic.venues.data.repository;

import android.support.annotation.NonNull;

import com.wulikic.venues.data.model.SearchByLocationNameResponse;

import io.reactivex.Observable;

/**
 * Created by vesna on 19/08/2017.
 */

public interface SearchRepository {

    Observable<SearchByLocationNameResponse> searchByName(@NonNull String locationName);

}
