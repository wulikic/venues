package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vesna on 19/08/2017.
 */

public class ResponseData {

    @SerializedName("geocode")
    GeocodeData geocode;

    @SerializedName("totalResults")
    int totalResults;

    @SerializedName("groups")
    List<VenueGroupData> groups;

    public GeocodeData getGeocode() {
        return geocode;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<VenueGroupData> getGroups() {
        return groups;
    }

}
