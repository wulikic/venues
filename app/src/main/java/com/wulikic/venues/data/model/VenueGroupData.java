package com.wulikic.venues.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vesna on 19/08/2017.
 */

public class VenueGroupData {

    @SerializedName("items")
    List<VenueItemData> venueItems;

    public List<VenueItemData> getVenueItems() {
        return venueItems;
    }

}
