package com.wulikic.venues.data;

import com.wulikic.venues.data.model.SearchByLocationNameResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by vesna on 19/08/2017.
 */

public interface FoursquareApi {

    @GET("/v2/venues/explore")
    Observable<SearchByLocationNameResponse> searchByLocationName(@Query(value = "near") String locationName);

}
